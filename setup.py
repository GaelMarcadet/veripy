from setuptools import setup

PACKAGE_NAME  = 'verilib'
VERSION = '0.1'
DESCRIPTION = 'Parallelized systems modeling and verification tool'
AUTHOR = 'Unnamed Team'
PACKAGES = [
    'verilib'
]

DEPENDANCIES = [
]

setup(
    name = PACKAGE_NAME,
    version = VERSION,
    description = DESCRIPTION,
    author = AUTHOR,
    packages = PACKAGES,
    install_requires = DEPENDANCIES,
)