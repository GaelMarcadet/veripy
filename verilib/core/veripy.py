# =================================================
# File: veripy.py
# Author: Gael Marcadet
# Description: Synchronized systems verification
# library.
# =================================================



# -------------------------------------------------
# File writer: Tool used to easily write in file.
# -------------------------------------------------

SMART_WRITER_READ = "w+"
SMART_WRITER_ADD = "a"
CURLY_BRACKETS_OPEN = '\u007b'
CURLY_BRACKETS_CLOSE = '\u007d'

class file_writer:
    '''
    File writer class is a tool used to enhanced writing in file.
    Some features like indent are designed to add flexibility.

    This class used a buffer to store data and limit interactions with file.
    '''
    def __init__( self, filename, mode = SMART_WRITER_READ ):
        self.mode = mode
        self.filename = filename
        self.__indent = 0
        self.buff = []

    def clear( self ):
        ''' Clears buffer. All data won't be accesible anymore.'''
        self.buff = ""

    def write_line( self, message ):
        ''' Writes a line in buffer. '''
        m = "".join([ self.__create_indents(), str(message), "\n" ])
        self.buff.append( m )
    
    def next_line( self ):
        ''' Jumps to the next line, according with current indent. '''
        self.buff.append( "\n" )

    def push_indent( self ):
        ''' Increases current indent. '''
        self.__indent += 1
    
    def pull_indent( self ):
        ''' Decreases current indent. It cannot be lower than 0. '''
        self.__indent = max( self.__indent - 1, 0 )
    
    def indent( self, new_indent : int ):
        ''' Places indent at a specific position. '''
        if new_indent < 0:
            raise Exception( f"Illegal indentation: Must be positive indent, got {new_indent}" )
        self.__indent = new_indent

    def flush( self, clear_buffer : bool = False ):
        ''' Writes buffer in specified file. If required, buffer is cleared. '''
        # writing buffer in specified file
        file = open( self.filename, SMART_WRITER_READ )
        file.write( "".join( self.buff ) )
        file.close()

        # clear buffer if required
        if clear_buffer:
            self.clear()
        

    def __create_indents( self ) -> str:
        ''' Creates a textual indent. '''
        return "\t" * self.__indent

# -------------------------------------------------
# Systems: Classes to manipulate systems.
# -------------------------------------------------

class system:
    ''' 
    System class allows to manipulate easily system defined as a 
    set of states, transitions between them, and properties.
    '''
    def __init__( self, name : str, nb_states : int, init : int = 0 ):
        self.init = init
        self.name = name
        self.nb_states = nb_states
        self.__states = range( nb_states )
        self.props = {}
        for sys_philo in range( nb_states ):
            self.props[ sys_philo ] = []

        self.trans = []

    def __eq__( self, o ):
        ''' Returns True if o is a system and has the name, False otherwise. '''
        return isinstance( o, system ) and o.name == self.name

    def __copy__( self ):
        ''' Creates and returns a deep copy of this system. '''
        cp_sys = system( self.name, self.nb_states, self.init )
        cp_sys.props = self.props.copy()
        cp_sys.trans = self.trans.copy()
        return cp_sys

    def __str__( self ):
        ''' Creates and returns a textual version of this system. '''
        return f'[{self.name} with {self.nb_states} states from {self.init}, props: {self.props}, trans: {self.trans}]'

        
    def add_prop( self, prop : str, *arg_states ):
        ''' Adds a property at the specified states. '''
        # cannot add empty property
        if prop == '':
            return

        # browse states collections and add them one by one.
        # Collection can be a single int or an iterable set of int.
        for arg_state in arg_states:
            if type( arg_state ) == int:
                states = [ arg_state ]
            else:
                # try to iterate on states
                try:
                    for _ in arg_state:
                        break
                    states = arg_state
                except:
                    raise Exception( 'Invalid argument: Cannot manipulate states' )

            
            for state in states:
                if state in self.props:
                    state_props = self.props[ state ]
                    if prop not in state_props:
                        self.props[ state ].append( prop )
                else:
                    raise Exception( f"Cannot add property {prop} at undefined state {state}" )
    

            

    def add_trans( self, src, dest, name = '' ):
        ''' Adds transition between two states. '''
        if not self.__valid_state( src ) or not self.__valid_state( dest ):
            raise Exception( f"Illegal state for transition {src} -> {dest} with name \"{name}\"" )
        
        # adds transition only if she doesn't exist yet.
        for e_src, e_dest, e_name in self.trans:
            if e_src == src and e_dest == dest and e_name == name:
                return
        
        self.trans.append( ( src, dest, name ) )

    def contains_trans( self, trans : str ):
        ''' Returns True if transition name exists, False otherwise. '''
        for _, _, name in self.trans:
            if name == trans:
                return True
        return False
        
    def write_buffer( self, writer : file_writer ):
        ''' Write system into buffer. '''
        # computing header
        s_header_props = ", ".join( self.get_props() )
        s_header_title = self.name
        
        # write header
        writer.write_line( f"{s_header_title} = [{s_header_props}] {CURLY_BRACKETS_OPEN}" )
        
        # write system body
        writer.push_indent()
        writer.write_line( f"etat = {self.nb_states};" )
        writer.write_line( f"init = {self.init};" )
        writer.next_line()
        
        # printing states properties
        for state in self.__states:
            s_state_props = ", ".join( self.props[ state ] )
            if s_state_props != "":
                writer.write_line( f"{state} = {s_state_props};" )

        writer.next_line()
        for src, dest, name in self.trans:
            core_trans = f"{src} -> {dest}"
            trans_name = f'[{name}]' if name  != '' else ''
            writer.write_line( f'{core_trans} {trans_name};' )


        # finish system writting
        writer.pull_indent()
        writer.write_line( f'{CURLY_BRACKETS_CLOSE};;' )
        writer.next_line()
    

    def get_props_by_states( self ):
        ''' Returns properties indexed by states. '''
        return self.props
    
    def get_props( self ):
        ''' Returns properties. '''
        list_props = []
        for _, props in self.props.items():
            list_props += props
        return list(dict.fromkeys( list_props ))


    def __valid_state( self, state ):
        return state in self.__states




TRANSITION_NOTHING = "_"
class synchronized_system:
    '''
    Allows to manipulate synchronized system
    '''
    def __init__( self, name ):
        self.name = name
        self.systems = {}
        self.__trans_rules = [] 
        self.__actions = []

    def __copy__( self ):
        ''' Creates and returns a deep copy of system. '''
        cp_sync = synchronized_system( self.name )
        cp_sync.systems = self.systems.copy()
        cp_sync.__trans_rules = self.__trans_rules.copy()
        cp_sync.__actions = self.__actions.copy()
        return cp_sync

    def path( self, target : str ):
        ''' Adds an instruction to create a path from initial state to the target. '''
        self.__actions.append( f"{self.name} = {self.name} -> {target};;" )


    def todot( self, filename : str ):
        ''' Adds an instruction to export synchronized system in .dot file. '''
        import re
        todot_pattern = re.compile( "^.+.dot$" )
        if todot_pattern.match( filename ):
            self.__actions.append( f'todot {filename} {self.name};;' )
        else:
            self.__actions.append( f'todot {filename}.dot {self.name};;' )

    def reduce( self, prop, red_sys : str = None ):
        ''' Adds an instruction to reduce synchronized system and keep only 
        states which verify specified property. '''
        if red_sys is None:
            red_sys = self.name
        
        if red_sys != self.name:
            from copy import copy
            sys = copy( self )
        else:
            sys = self

        sys.__actions.append( f'{sys.name} = {self.name} | {prop};;' )
        return sys

    def tag( self, tag : str, states : str ):
        ''' Adds an instruction to tag states which verify specified property. '''
        self.__actions.append( f'{self.name} += {tag} <- {states};;' )

    def add_system( self, sys_name, sys : system ):
        ''' Adds a named system to synchronized system. '''
        if isinstance( sys, system ):
            from copy import copy
            self.systems[ sys_name ] = copy( sys )
        else:
            raise Exception( "Illegal parameter: expected system argument" )

    def add_trans( self, name, rules ):
        ''' 
        Adds a named transition into synchronized system. 
        A rule is a property that a system must verified to accept transition.
        System and property are stucked together, separated by two dots ':'.
        Multiples rules can be specified once by separating them with virgula ','.

        >>> syncro = synchronized_system( 'test' )
        >>> syncro.add_system( 'sys', my_system )
        >>> syncro.add_trans( 'valid_trans_single', 'sys:property' )
        >>> syncro.add_trans( 'valid_trans_multiple', 'sys1:prop,sys2:prop' )
        '''
        def extract_rule( s_rules : str ):
            rules = {}
            for s_rule in s_rules.split( ',' ):
                try:
                    s_rule = s_rule.split( ':' )
                    sys_name = s_rule[ 0 ].strip()
                    sys_state = s_rule[ 1 ].strip()
                    rules[ sys_name ] = sys_state
                except:
                    pass
            return rules

        if type( rules ) == str:
            rules = extract_rule( rules )
        
        # checks rules integrity
        registered_sys_names = self.systems.keys()
        for sys_name, sys_state in rules.items():

            # system's name must be already registered before used
            if sys_name not in registered_sys_names:
                raise Exception( f"Invalid transition: '{sys_name}' is not registered." )
            
            # system's state must exists in system
            if not self.systems[ sys_name ].contains_trans( sys_state ):
                raise Exception( f"Invalid transition: '{sys_state}' not exist in system '{sys_name}'." )

        self.__trans_rules.append( ( name, rules ) )

    def action( self, action : str ):
        ''' Adds raw instruction. '''
        self.__actions.append( f'{action};;' )


    def write_buffer( self, writer : file_writer ):
        ''' Writes synchronized system in buffer. '''
        # exporting system head
        s_sys_names = ", ".join([ f'{sys.name} {sys_name}' for sys_name, sys in self.systems.items() ])
        writer.write_line( f"{self.name} = <{s_sys_names}> {CURLY_BRACKETS_OPEN}" )

        # exporting sytem transitions
        writer.push_indent()
        
        for rule_name, rule in self.__trans_rules:
            # define systems states
            states = []
            for sys_name, sys in self.systems.items():
                if sys_name in rule:
                    states.append( rule[ sys_name ] )
                else:
                    states.append( TRANSITION_NOTHING )
        
            # export transition in buffer
            s_trans = ", ".join( states )
            writer.write_line( f'<{s_trans}> -> {rule_name};' )

        writer.pull_indent()

        writer.write_line( f'{CURLY_BRACKETS_CLOSE};;' )



    def write_file( self, filename : str ):
        """ Exports automatically all dependant systems and synchronized system itself. """
        exported_sys = []
        for sys_name, sys in self.systems.items():
            found = False
            for exp_sys in exported_sys:
                if sys.name == exp_sys.name:
                    found = True
                    break
            if not found:
                exported_sys.append( sys )
    
        
        writer = file_writer( filename )
        for exp_sys in exported_sys:
            exp_sys.write_buffer( writer )

        # exporting system head
        s_sys_names = ", ".join([ f'{sys.name} {sys_name}' for sys_name, sys in self.systems.items() ])
        writer.write_line( f"{self.name} = <{s_sys_names}> {CURLY_BRACKETS_OPEN}" )

        # exporting sytem transitions
        writer.push_indent()
        
        for rule_name, rule in self.__trans_rules:
            # define systems states
            states = []
            for sys_name, sys in self.systems.items():
                if sys_name in rule:
                    states.append( rule[ sys_name ] )
                else:
                    states.append( TRANSITION_NOTHING )
        
            # export transition in buffer
            s_trans = ", ".join( states )
            writer.write_line( f'<{s_trans}> -> {rule_name};' )

        writer.pull_indent()

        writer.write_line( f'{CURLY_BRACKETS_CLOSE};;' )


        # write actions
        for action in self.__actions:
            writer.write_line( action )

        writer.flush()
    



def mutex( init : int = 1, mut_name : str = 'mutex' ) -> system:
    ''' Creates and returns a mutex system. '''
    mutex = system( mut_name, nb_states = 2, init = init )

    # mutex has two properties: available with 1 or busy to 0
    mutex.add_prop( 'available', 1 )
    mutex.add_prop( 'busy', 0 )

    # mutex has two transitions: P to lock mutex and V to unlock
    mutex.add_trans( 1, 0, 'P' )
    mutex.add_trans( 0, 1, 'V' )

    mutex.add_trans( 0, 0, 'is_P' )
    mutex.add_trans( 1, 1, 'is_V' )

    return mutex

def counter( 
    nstates : int, 
    steps, increase : 
    bool = True, 
    decrease : bool = True, 
    ctr_name = 'counter',
    zero_name : str = 'zero',
    fill_name : str = 'fill',
    init : int = 0 ) -> system:
    ''' Creates and returns a counter system. '''

    # nstates integrity verification
    if type( nstates ) != int: 
        raise Exception( 'Invalid type argument: Expected int' )

    if nstates < 0:
        raise Exception( f'Invalid argument: Cannot create counter with negative states number: {nstates}.' )
    

    # steps integrity verification
    if type( steps ) == int:
        steps = [ steps ]
    elif type( steps ) == list:
        if len( steps ) == 0 or type( steps ) != int:
            raise Exception( 'Invalid argument: Steps must be int or iterable of int.' )
    
    # initialize counter with properties
    counter = system( ctr_name, nb_states = nstates, init = init )
    counter.add_prop( zero_name, 0 )
    counter.add_prop( fill_name, nstates - 1 )

    # loading counter with incrementations and decrementation
    for step in steps:
        if increase:
            curr_state = 0
            for next_state in range( curr_state + step, nstates, step ):
                counter.add_trans( curr_state, next_state, f'inc{step}' )
                curr_state = next_state
    
        if decrease:
            curr_state = nstates - 1
            for next_state in range( curr_state - step, -1, -step ):
                counter.add_trans( curr_state, next_state, f'dec{step}' )
                curr_state = next_state

    return counter

def semaphore( 
    nstates : int, 
    init : int, 
    sem_name : str = 'semaphore', 
    available_name : str = 'available', 
    busy_name : str = 'busy'
    ):

    ''' Semaphore with n states wich identify all states different of 0 as available '''

    # initialize semaphore with properties available and busy
    semaphore = system( sem_name, nstates, init )
    semaphore.add_prop( busy_name, range( 0, nstates ) )

    # insert increasing transitions
    curr_state = 0
    for next_state in range( curr_state + 1, nstates, 1 ):
        semaphore.add_trans( curr_state, next_state, 'P' )
        curr_state = next_state
    
    # insert decreasing transitions
    curr_state = nstates - 1
    for next_state in range( curr_state - 1, -1, -1 ):
        semaphore.add_trans( curr_state, next_state, 'V' )
        curr_state = next_state
        
    return semaphore

def export_sys( sys : system, filename : str ):
    ''' Exports a system in a file '''
    writer = file_writer( filename )
    sys.write_buffer( writer )
    writer.flush()

def export_sync_sys( sync_sys : synchronized_system, filename : str ):
    ''' Exports synchronized system and all dependant systems in file '''
    sync_sys.write_file( filename )

def deadlock() -> str:
    return "AX(false)"

def EF( state ):
    return f"EF( {state} )"

def EX( state ):
    return f'EX( {state} )'

def AX( state ):
    return f'AX( {state} )'

def AF( state ):
    return f'AF( {state} )'

def AG( state ):
    return f'AG( {state} )'

def EG( state ):
    return f'EG( {state} )'

if __name__ == "__main__":
    sync = synchronized_system( "Test" )
    sync.add_system( "mut", mutex() )
    sync = sync.reduce( "BUG" )
    sync.todot( "test.dot" )
    sync.path( "p1.disponible" )
    sync.write_file( "test" )
    