# ===================================
# File: algo1_path.py
# Author: Gael Marcadet
# Description: mso files generator for starving-free problem. (algo1 in path)
# ===================================

from verilib.core.veripy import synchronized_system, AG, AF, EF, deadlock
from verilib.core.philib import philo, edge, weak_equity, strong_equity, insecure_states
from verilib.core.interact import input_string

def generate_philo_chemin( n_philo : int, filename : str ):
    '''
    Generates mso file for starving-free problem, with disktra idea.
    '''
    # can only build system with at least two philosophers.
    if n_philo < 2:
        raise Exception("[!] Invalid argument: Can build a synchronized system with two or more philosoph")

    syncro = synchronized_system( "System" )

    # adding philosphers into syncronized system
    sys_philo = philo()
    for i in range( 1, n_philo +1 ):
        syncro.add_system( f"p{i}", sys_philo )

    # adding edges into synchronized system
    sys_edge = edge()
    for i in range( 1, n_philo ):
        syncro.add_system( f'e{i}{i+1}', sys_edge )


    # first philosphe transitions
    # in path graph, first and last philosophers are simplier than other, due to their extremity.
    syncro.add_trans( "p1_hungry", f'p1:hungry,p2:is_think' )
    syncro.add_trans( "p1_hungry", f'p1:hungry,p2:not_think,e12:right' )
    syncro.add_trans( "p1_eat", f'p1:eat,e12:icenter' )
    syncro.add_trans( "p1_eat", f'p1:eat,e12:ileft' )
    syncro.add_trans( "p1_think", f'p1:think,e12:center' )

    # All philospohe without first and last
    for curr in range( 2, n_philo ):
        prev, next = curr - 1, curr + 1

        edge_pc = f'e{prev}{curr}'
        edge_cn = f'e{curr}{next}'

        # hungry transitions
        trans_hungrt_name = f"p{curr}_hungry"
        syncro.add_trans( trans_hungrt_name, f'p{curr}:hungry,p{prev}:is_think,p{next}:is_think' )
        syncro.add_trans( trans_hungrt_name, f'p{curr}:hungry,p{prev}:not_think,p{next}:is_think,{edge_pc}:left' )
        syncro.add_trans( trans_hungrt_name, f'p{curr}:hungry,p{prev}:is_think,p{next}:not_think,{edge_cn}:right' )
        syncro.add_trans( trans_hungrt_name, f'p{curr}:hungry,p{prev}:not_think,p{next}:not_think,{edge_pc}:left,{edge_cn}:right' )

        # eat transitions
        trans_eat_name = f'p{curr}_eat'
        syncro.add_trans( trans_eat_name, f'p{curr}:eat,{edge_pc}:icenter,{edge_cn}:icenter' )
        syncro.add_trans( trans_eat_name, f'p{curr}:eat,{edge_pc}:iright,{edge_cn}:icenter' )
        syncro.add_trans( trans_eat_name, f'p{curr}:eat,{edge_pc}:icenter,{edge_cn}:ileft' )
        syncro.add_trans( trans_eat_name, f'p{curr}:eat,{edge_pc}:iright,{edge_cn}:ileft' )

        # think transitions
        trans_think_name = f'p{curr}_think'
        syncro.add_trans( trans_think_name, f'p{curr}:think,{edge_pc}:center,{edge_cn}:center' )

    # last philosphe transitions
    # in path graph, first and last philosophers are simplier than other, due to their extremity.
    last_p = f'p{n_philo}'
    edge_pc = f'e{n_philo-1}{n_philo}'
    syncro.add_trans( f"{last_p}_hungry", f'p{n_philo}:hungry,p{n_philo-1}:is_think' )
    syncro.add_trans( f"{last_p}_hungry", f'p{n_philo}:hungry,p{n_philo-1}:not_think,{edge_pc}:left' )
    syncro.add_trans( f"{last_p}_eat", f'p{n_philo}:eat,{edge_pc}:icenter' )
    syncro.add_trans( f"{last_p}_eat", f'p{n_philo}:eat,{edge_pc}:iright' )
    syncro.add_trans( f"{last_p}_think", f'p{n_philo}:think,{edge_pc}:center' )


    # tag nodes which verify deadlocks property
    syncro.tag( "DEAD", AG( deadlock() ) )
    
    for i_philo in range( 1, n_philo + 1 ):
        p = f'p{i_philo}'
        # tag nodes which verify weak equity propery
        syncro.tag( f"WEAK_EQUITY_P{i_philo}", AG( weak_equity( p ) ) )
        
        # tag nodes which verify strong equity property
        syncro.tag( f"STRONG_EQUITY_P{i_philo}", AG( strong_equity( p ) ) )

    # tag nodes which verify same time eating property.
    syncro.tag( "INSECURE", AG( insecure_states( n_philo, is_cyclic = False ) ) )

    # generating first todot with all previous properties
    syncro.todot( "chemin.dot" )

    # write syncronized system in filename
    syncro.write_file( filename )


if __name__ == "__main__":
    try:
        n_philo = int(input("> Input number of philo: "))
        filename = input_string( "Input mso filename", default="algo1_path.mso" )
        generate_philo_chemin( n_philo, filename )
        print( f"File {filename} generated successfully" )
    except Exception as e:
        print( "[!] An error occured: " + str(e) )


            